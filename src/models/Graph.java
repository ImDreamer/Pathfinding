package models;

import java.util.ArrayList;
import java.util.List;

public class Graph {
    private final List<Vertex> vertices;
    private final List<Edge> edges;

    public Graph() {
        vertices = new ArrayList<>();
        edges = new ArrayList<>();
    }

    public List<Vertex> GetVertexes() {
        return vertices;
    }

    public List<Edge> GetEdges() {
        return edges;
    }

    /***
     * Add a edge.
     * @param edgeId The id of the edge
     * @param sourceVertex The source vertex
     * @param destinationVertex The destination vertex
     * @param weight The weight of the edge
     */
    public void AddEdge(String edgeId, int sourceVertex, int destinationVertex, int weight) {
        edges.add(new Edge(edgeId, vertices.get(sourceVertex), vertices.get(destinationVertex), weight));
    }

    /**
     * Add the amount of Vertices.
     *
     * @param amount The amount of Vertices.
     * @remarks It's exclusive and starts at zero.
     */
    public void AddVertices(int amount) {
        for (int i = 0; i < amount; i++) {
            vertices.add(new Vertex("Vertex_" + i));
        }

    }

}