package models;

public class Edge {
    private final String id;
    private final Vertex source;
    private final Vertex destination;
    private final int weight;

    Edge(String id, Vertex source, Vertex destination, int weight) {
        this.id = id;
        this.source = source;
        this.destination = destination;
        this.weight = weight;
    }

    public Vertex GetDestination() {
        return destination;
    }

    public Vertex GetSource() {
        return source;
    }

    public int GetWeight() {
        return weight;
    }
}