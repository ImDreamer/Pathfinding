package main;

import algorithm.Dijkstra;
import models.Graph;
import models.Vertex;

import java.util.LinkedList;

public class Main {
    public static void main(String[] args) {
        Graph graph = new Graph();
        graph.AddVertices(8);

        graph.AddEdge("Edge_0", 1, 2, 85);
        graph.AddEdge("Edge_1", 1, 3, 217);
        graph.AddEdge("Edge_2", 1, 4, 173);
        graph.AddEdge("Edge_3", 2, 7, 186);
        graph.AddEdge("Edge_4", 2, 3, 103);
        graph.AddEdge("Edge_5", 3, 4, 250);
        graph.AddEdge("Edge_6", 4, 5, 84);
        graph.AddEdge("Edge_7", 5, 6, 167);
        graph.AddEdge("Edge_8", 6, 7, 502);

        Dijkstra dijkstra = new Dijkstra(graph);
        dijkstra.Execute(graph.GetVertexes().get(1));
        LinkedList<Vertex> path = dijkstra.GetPath(graph.GetVertexes().get(6));

        System.out.println("The shortest path is: ");
        for (Vertex v : path) {
            System.out.println(v);
        }
    }
}
